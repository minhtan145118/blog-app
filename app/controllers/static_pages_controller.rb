class StaticPagesController < ApplicationController
  def home
  end

  def new
    if session[:user_id]
      flash[:success] = 'you have already signed in'
      redirect_to root_path
    else
      @user = User.new
    end
  end

  def create
    @user = User.find_by email: params[:user][:email]
    if @user.present?
      if @user.authenticate(params[:user][:password])
        login(@user)
        flash[:success] = 'Sign in successfully'
        redirect_to root_path
      else
        flash.now[:danger] = 'Wrong password'
        render :new
      end
    else
      @user = User.new user_params
      flash.now[:danger] = 'Cant find user with email'
      render :new
    end
  end

  def destroy
    session[:user_id] = nil
    flash[:suscess] = 'You are sign out successfully'
    redirect_to root_path
  end

  private

  def user_params
    params.require(:user).permit :email, :password
  end

end
