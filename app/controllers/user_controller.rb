class UserController < ApplicationController

  def new
    @user = User.new
  end

   def create
     @user = User.new user_params
     if @user.save
       UserMailer.welcome_email(@user).deliver
       flash[:success] = 'You are signed up suscessfully'
       redirect_to root_path
     else
       flash.now[:danger] = 'Something went wrong'
       render :new
     end
   end

  private

  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confimation)
  end

end
