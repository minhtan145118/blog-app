class UserMailer < ApplicationMailerz
  default from: 'notifications@example.com'

  def welcome_email(user)
    @user = user
    mail to: user.email, subject: 'Sign Up Conformation'
  end
end
