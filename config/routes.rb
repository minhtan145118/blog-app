Rails.application.routes.draw do
  get '/sign_up' => 'user#new', as: :sign_up
  post'/sign_up' => 'user#create'

  get '/sign_in' => 'static_pages#new', as: :sign_in
  post '/sign_in' => 'static_pages#create'

  root to: 'static_pages#home'

  delete '/sign_out' => 'static_pages#destroy', as: :sign_out
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
